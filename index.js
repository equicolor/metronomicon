var app = require('express')()
var http = require('http').Server(app)
var io = require('socket.io')(http)

let c = 1

app.get('/', (req, res) => {
  res.sendfile('index.html')
})

app.get('/click.ogg', (req, res) => {
  res.sendfile('click.ogg')
})

io.on('connection', socket => {
  console.log('=> Connect')

  socket.on('test', fn => {
	c++
	console.log('<-> Ping # ' + c)
	fn(c)
  })

  socket.on('play', () => {
  	console.log('|> Play')
  	io.emit('play', {
  		test: 123
  	})
  })

  socket.on('stop', () => {
  	console.log('[] Stop')
  	io.emit('stop', {
  		test: 123
  	})
  })

})

http.listen(3000, function(){
  console.log('listening on *:3000')
})

